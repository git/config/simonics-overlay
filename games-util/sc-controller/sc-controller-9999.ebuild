# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
PYTHON_COMPAT=( python2_7 )

SCM=""
if [ "${PV#9999}" != "${PV}" ] ; then
        SCM="git-r3"
        EGIT_REPO_URI="https://github.com/kozec/sc-controller"
fi
inherit ${SCM} distutils-r1

DESCRIPTION="User-mode driver and GTK3 based GUI for the Steam Controller"
HOMEPAGE="https://github.com/kozec/sc-controller"
if [ "${PV#9999}" != "${PV}" ] ; then
        SRC_URI=""
        KEYWORDS=""
else
	SRC_URI="https://github.com/kozec/${PN}/archive/v${PV}.tar.gz"
	KEYWORDS="x86 amd64"
fi

LICENSE="GPL2"
SLOT="0"
IUSE=""

RDEPEND="x11-libs/gtk+:3
		 dev-python/pygobject:3
		 dev-python/pycairo
		 dev-python/pylibacl"
DEPEND="${RDEPEND}
		dev-python/setuptools[${PYTHON_USEDEP}]"

src_prepare() {
	eapply "${FILESDIR}/udev-rules.patch" # use games group
	eapply "${FILESDIR}/desktop-name.patch" # change name in desktop file
	default
}
