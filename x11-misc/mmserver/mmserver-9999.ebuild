# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

[[ ${PV} = 9999 ]] && inherit git-r3
inherit cmake-utils

DESCRIPTION="Fork of Mobile Mouse Server for Linux."
HOMEPAGE="https://github.com/kiriakos/mmserver"
EGIT_REPO_URI="https://github.com/kiriakos/mmserver"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="zeroconf"

RDEPEND="
	dev-libs/libpcre
	dev-libs/libconfig
	x11-libs/gtk+:2
	x11-libs/libXtst
	x11-libs/libXt
	x11-libs/libXmu
	zeroconf? ( net-dns/avahi )
"
DEPEND="${RDEPEND}"

src_prepare() {
	if ! use zeroconf; then
		epatch "${FILESDIR}/no_avahi.patch"
	fi
	eapply_user
}
