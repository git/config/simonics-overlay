# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

inherit git-r3 qmake-utils

DESCRIPTION="A notification system for tiling window managers."
HOMEPAGE="https://github.com/sboli/Twmn"
EGIT_REPO_URI="https://github.com/sboli/twmn.git"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtdbus:5
	dev-qt/qtx11extras:5
	dev-libs/boost
	sys-apps/dbus
	"
RDEPEND="${DEPEND}"

DOCS=( TODO README.md )

src_unpack() {
	git-r3_src_unpack
}

src_prepare() {
	sed -i -e 's#/usr/local/#'${D}'/usr/#g' */*.pro
	eapply_user
}

src_configure() {
	eqmake5
}

src_install() {
    cat <<-EOF > "${T}"/org.freedesktop.Notifications.service
[D-BUS Service]
Name=org.freedesktop.Notifications
Exec=/usr/bin/twmnd
EOF

    insinto /usr/share/dbus-1/services
    doins "${T}"/org.freedesktop.Notifications.service
	default
}
