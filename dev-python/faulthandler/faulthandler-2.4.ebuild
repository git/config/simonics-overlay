# Copyright owners: Arfrever Frehtes Taifersar Arahesis
# Distributed under the terms of the GNU General Public License v2

EAPI="5"
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Display the Python traceback on a crash"
HOMEPAGE="https://faulthandler.readthedocs.org/ https://github.com/haypo/faulthandler https://pypi.python.org/pypi/faulthandler"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="dev-python/setuptools"
RDEPEND=""

DOCS="AUTHORS README"
