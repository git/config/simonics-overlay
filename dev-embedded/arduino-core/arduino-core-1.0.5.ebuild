# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-embedded/arduino/arduino-1.0.5.ebuild,v 1.3 2013/08/18 13:27:34 ago Exp $

EAPI=4
inherit eutils

DESCRIPTION="An open-source AVR electronics prototyping platform without the Java IDE"
HOMEPAGE="http://arduino.cc/ http://arduino.googlecode.com/"
SRC_URI="http://arduino.googlecode.com/files/arduino-${PV}-src.tar.gz"
LICENSE="GPL-2 GPL-2+ LGPL-2 CC-BY-SA-3.0"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="strip binchecks"
IUSE="doc examples"

DEPEND=""
RDEPEND="${DEPEND}
	!dev-embedded/arduino
	dev-embedded/avrdude
	dev-embedded/uisp
	sys-devel/crossdev"

S="${WORKDIR}/arduino-${PV}"

src_install()
{
	insinto "/usr/share/arduino/"
	doins -r hardware libraries
	fowners -R root:uucp "/usr/share/arduino/hardware"

	cd "${S}/build/shared" || die

	if use examples; then
		dodir "/usr/share/doc/${P}"
		cp -r examples/ "${D}/usr/share/doc/${P}/examples" || die
	fi

	if use doc; then
		dodoc revisions.txt "${S}"/readme.txt
		unzip reference.zip || die
		cp -r reference/ "${D}/usr/share/doc/${P}/reference" || die
	fi

	insinto "/usr/share/arduino/lib"
	doins -r lib/*.txt lib/theme lib/*.jpg
}

pkg_postinst() {
	[ ! -x /usr/bin/avr-g++ ] && ewarn "Missing avr-g++; you need to crossdev -s4 avr"
}
