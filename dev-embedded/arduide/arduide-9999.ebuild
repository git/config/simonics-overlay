# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
fi
inherit eutils

DESCRIPTION="Arduide is a Qt-based Arduino IDE"
HOMEPAGE="http://mupuf.org/project/arduide.html"
if [[ "${PV}" == "9999" ]]; then
	EGIT_REPO_URI="git://github.com/mupuf/arduide.git"
	KEYWORDS="amd64 x86"
else
	KEYWORDS="amd64 x86"
fi

LICENSE="GPL-2"
SLOT="0"
IUSE=""

if [[ ${PV} == 9999 ]]; then
	DEPEND="
		dev-util/cmake
	"
fi
RDEPEND="
	x11-libs/qscintilla
	dev-libs/grantlee:0
	virtual/libudev
	dev-qt/qtgui:4
"

src_prepare() {
	epatch "${FILESDIR}/includes.patch"
	epatch_user
}

src_configure() {
	cmake -DCMAKE_INSTALL_PREFIX=/usr -DUSE_FHS_PATHS=ON -DCMAKE_BUILD_TYPE=Release ./ || die "Configuration failed!"
}
