# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit git-r3

DESCRIPTION="slmenu is a dmenu clone for the console"
HOMEPAGE="https://sierra/git/simon/slmenu"
EGIT_REPO_URI="https://sierra/git/hub-mirror/slmenu.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}/config.mk.patch"
}
