# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

DESCRIPTION="Search APIs offline, in terminal or browser"
HOMEPAGE="https://sunaku.github.io/dasht/man/man0/README.html"

EGIT_REPO_URI="https://github.com/sunaku/dasht.git"
inherit git-r3

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	www-client/w3m
	dev-db/sqlite:3
"

RDEPEND="${DEPEND}"

src_install() {
	dobin bin/dasht
	dobin bin/dasht-docsets
	dobin bin/dasht-docsets-install
	dobin bin/dasht-docsets-remove
	dobin bin/dasht-docsets-update
	dobin bin/dasht-query-exec
	dobin bin/dasht-query-html
	dobin bin/dasht-query-line
	dobin bin/dasht-server
	dobin bin/dasht-server-http 

	doman man/man1/*.1

	dodoc README.md
	dodoc VERSION.md 
}
