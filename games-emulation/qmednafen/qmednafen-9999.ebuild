# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit qmake-utils git-r3

EGIT_REPO_URI="https://github.com/Tuxman88/QMednafen.git"
KEYWORDS=""

DESCRIPTION="qmednafen"
HOMEPAGE="https://github.com/Tuxman88/QMednafen/"

LICENSE="GPL-2"
SLOT="0"
IUSE=""

DEPEND="dev-qt/qtcore
	games-emulation/mednafen
"

src_configure() {
	eqmake5
	make all
}

src_install() {
	make install
}
