# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Create audio files (mp3/ogg/flac/m4a/wav) from an Audio CD."
HOMEPAGE="http://www.suwald.com/ripit/"
SRC_URI="http://www.suwald.com/ripit/${P}.tar.bz2"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="cdparanoia dagrab lame vorbis flac faac"
DEPEND="
	dev-perl/CDDB_get
	dev-perl/MP3-Tag
	dev-perl/WebService-MusicBrainz
	dev-perl/MusicBrainz-DiscID
"
RDEPEND="
	cdparanoia? ( media-sound/cdparanoia )
	dagrab? ( media-sound/dagrab )
	lame? ( >=media-sound/lame-3.98.2 )
	vorbis? ( media-sound/vorbis-tools )
	flac? ( media-libs/flac )
	faac? ( media-libs/faac )
"

src_install() {
	make DESTDIR="${D}" prefix="${D}/usr/" install || die "make install failed"
}
