# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit linux-info

DESCRIPTION="Console-based Audio Visualizer for ALSA (=CAVA)"
HOMEPAGE="https://github.com/karlstav/cava"
SRC_URI="https://github.com/karlstav/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	sci-libs/fftw
	dev-libs/iniparser:4
"
RDEPEND="${DEPEND}"

pkg_setup() {
	if linux_config_exists ; then
		einfo "Checking kernel configuration at $(linux_config_path)..."
		if ! linux_chkconfig_present SND_ALOOP ; then
			ewarn 'Kernel option CONFIG_SND_ALOOP=[ym] needed but missing'
		fi
	fi
}

src_compile() {
	sed -i -e 's/ncursesw5-config/ncursesw6-config/' makefile
	emake -j1 || die
}

src_install() {
	dodoc README.md
	emake DESTDIR="${D}" PREFIX=/usr install || die
}
