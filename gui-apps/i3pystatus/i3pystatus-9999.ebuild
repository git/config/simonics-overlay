# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

PYTHON_COMPAT=( python3_{7,8,9} )

inherit python-r1 distutils-r1 git-r3

DESCRIPTION="Replacement for i3status"
HOMEPAGE="http://docs.enkore.de/i3pystatus"
EGIT_REPO_URI="https://github.com/enkore/i3pystatus.git"
EGIT_BRANCH="current"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="alsa dbus psutil network parcel"
# IUSE="weather wifi pulseaudio"

DEPEND=""
RDEPEND="
	${PYTHON_DEPS}
	alsa? ( dev-python/pyalsaaudio )
	dbus? ( dev-python/dbus-python )
	psutil? ( dev-python/psutil )
	network? ( dev-python/netifaces )
	parcel? ( dev-python/beautifulsoup:4 dev-python/cssselect dev-python/lxml )"
# RDEPEND="
# 	weather? ( dev-python/pywapi )
# 	wifi? ( dev-python/netifaces dev-python/basiciw )
# 	pulseaudio? ( dev-python/colour )"

S="${WORKDIR}/${PN}-9999"

src_prepare() {
	# patches all round
	#epatch "${FILESDIR}/${P}-openvpn.patch"
	epatch "${FILESDIR}/${P}-nowplaying.patch"
	epatch "${FILESDIR}/${P}-stripping.patch"
	epatch "${FILESDIR}/${P}-shell.patch"
	epatch "${FILESDIR}/${P}-network.patch"
}
